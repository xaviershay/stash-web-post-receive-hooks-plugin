(function($){

        //
        // functions
        //

        function removeUrl (element) {
            var $container = $('.web-post-hooks');
            $(element).parent().remove();
    
            var $visibleElements = $container.find('.web-post-hook');
            // if just one URL input remains, lets disable "remove" button beside
            if ($visibleElements.length == 1) {
                $visibleElements.children(".delete-button").addClass('web-post-hook-hidden');
            }
            // re-number inputs
            $visibleElements.each(function(index, visibleElement) {
                var $currentElement = $(visibleElement);
                $currentElement.attr('id', 'web-post-hook-' + index);
                $currentElement.find('label').attr('for', 'hook-url-' + index);
                $currentElement.find('input').attr('id', 'hook-url-' + index)
                                             .attr('name', 'hook-url-' + index);
            });
            // in case control Add is hidden
            var $controlAdd = $container.find('#web-post-hook-add');
            $controlAdd.removeClass('web-post-hook-hidden');
        };

        function addUrl (element) {
            var maxInputs = parseInt($(element).attr('data-max-inputs'), 10);
            var $container = $('.web-post-hooks');
            var $controlAdd = $container.find('#web-post-hook-add');
            var $existingInputs = $container.children('.web-post-hook');
            
            var html = stash.webhooks.form.anotherUrl({
                'canDelete' : true,
                'count' : $existingInputs.length,
                'value' : ''
            });
            $(html).insertBefore($controlAdd.parent());

            // hide Add link if we're on the top of the limit
            $controlAdd.toggleClass('web-post-hook-hidden', $existingInputs.length + 1 === maxInputs);

            // if adding another URL input, we need to enable first "remove" button
            $container.find('.delete-button').removeClass('web-post-hook-hidden');
            
        };

        //
        // event bindings
        //

        $(document).on('click', '.web-post-hook-add', function(e) {
            e.preventDefault();
            addUrl(this);
        });

        $(document).on('click', '.web-post-hook-delete', function (e) {
            e.preventDefault();
            removeUrl(this);
        });

}(AJS.$));
package com.atlassian.stash.plugin.hook.internal;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.URI;
import java.util.Collection;

import javax.annotation.Nonnull;

import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.user.StashUser;

/**
 * Request for processing realized by {@link RequestFactoryWebHookProcessorWorker}.
 */
public class WebHookRequest {

	private final URI hookUrl;
	private final Repository repository;
	private final Collection<RefChange> refChanges;
	private final RestPostReceiveHookSupplier restPostReceiveHook;

	public WebHookRequest(@Nonnull SecurityService securityService, @Nonnull HistoryService historyService, @Nonnull NavBuilder navBuilder,
			int changesetsLimit, int changesLimit, @Nonnull Repository repository, @Nonnull Collection<RefChange> refChanges,
			@Nonnull URI hookUrl, StashUser currentUser) {
		checkNotNull(securityService);
		checkNotNull(historyService);
		checkNotNull(navBuilder);

		checkNotNull(repository);
		checkNotNull(hookUrl);
		checkNotNull(refChanges);

		this.hookUrl = hookUrl;
		this.repository = repository;
		this.refChanges = refChanges;
		this.restPostReceiveHook = new RestPostReceiveHookSupplier(securityService, historyService, navBuilder, changesetsLimit,
				changesLimit, repository, refChanges, currentUser);
	}

	@Nonnull
	public URI getHookUri() {
		return hookUrl;
	}

	@Nonnull
	public Repository getRepository() {
		return repository;
	}

	@Nonnull
	public Collection<RefChange> getRefChanges() {
		return refChanges;
	}

	@Nonnull
	public RestPostReceiveHookSupplier getRestPostReceiveHook() {
		return restPostReceiveHook;
	}

}

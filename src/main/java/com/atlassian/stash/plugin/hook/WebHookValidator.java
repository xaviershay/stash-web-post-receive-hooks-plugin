package com.atlassian.stash.plugin.hook;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.setting.RepositorySettingsValidator;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;

public class WebHookValidator implements RepositorySettingsValidator {
	
	private static final int MAX_HOOKS = 5;
	private static Pattern HTTP_PROTOCOL_PATTERN = Pattern.compile("(?i)(http(s)?)");
	private I18nService i18n;

	public WebHookValidator(I18nService i18n) {
		this.i18n = i18n;
	}

	@Override
	public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors errors, @Nonnull Repository repo) {

		if (settings.asMap().size() > MAX_HOOKS) {
			errors.addFormError(i18n.getText("stash.webhooks.error.limit", "Only {0} hooks are allowed.", MAX_HOOKS));
		}

		Set<String> keySet = settings.asMap().keySet();
		for (String param : keySet) {
			String url = settings.getString(param);
			if (url == null || url.trim().equals("")) {
				
				errors.addFieldError(param, i18n.getText("stash.webhooks.error.required", "Field is required."));
			
			} else {
				try {
					
					URI uri = new URI(url);
					if (uri.getScheme() == null || !HTTP_PROTOCOL_PATTERN.matcher(uri.getScheme()).matches()) {
						errors.addFieldError(param, i18n.getText("stash.webhooks.error.http", "This needs to be an HTTP address."));
					}
					if (uri.getHost() == null) {
					    errors.addFieldError(param, i18n.getText("stash.webhooks.error.url", "Invalid URL."));
					}
					if (isDuplicated(url, settings.asMap())) {
						errors.addFieldError(param, i18n.getText("stash.webhooks.error.duplicity", "Duplicated URL."));
					}
					
				} catch (URISyntaxException e) {
					errors.addFieldError(param, i18n.getText("stash.webhooks.error.url", "Invalid URL."));
				}
			}
		}
	}

	private boolean isDuplicated(String url, Map<String, Object> values) {
	    boolean alreadyContains = false;
	    for (String param : values.keySet()) {
	        Object paramValue = values.get(param);
			if (paramValue != null && paramValue.equals(url)) {
				if (alreadyContains) {
					return true;
				}
				alreadyContains = true;
			}
        }
		return false;
    }
}

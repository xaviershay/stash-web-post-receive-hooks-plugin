package com.atlassian.stash.plugin.hook.internal;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.stash.plugin.hook.PostReceiveHookConstants;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.util.concurrent.ThreadFactories;
import com.atlassian.util.concurrent.ThreadFactories.Type;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Processes {@link WebHookRequest}-s.
 * 
 * It takes a {@link WebHookRequest} and adds it into the queue. Queue is prioritized in FIFO way. Queue is of limited size, and if queue
 * became full, than oldest one will be discarded.
 */
public class RequestFactoryWebHookProcessor implements DisposableBean {

	private static final Logger log = LoggerFactory.getLogger(RequestFactoryWebHookProcessor.class);

	private final RequestFactory<Request<?, Response>> requestFactory;
	private final ThreadPoolExecutor threadPoolExecutor;
	private final int connectionTimeout;
	private final ApplicationProperties applicationProperties;

	public RequestFactoryWebHookProcessor(ApplicationPropertiesService applicationPropertiesService,
			RequestFactory<Request<?, Response>> requestFactory, ApplicationProperties applicationProperties) {
		this(applicationPropertiesService, requestFactory, applicationProperties, buildExecutor(applicationPropertiesService));
	}

	@VisibleForTesting
	protected RequestFactoryWebHookProcessor(ApplicationPropertiesService applicationPropertiesService,
			RequestFactory<Request<?, Response>> requestFactory, ApplicationProperties applicationProperties,
			ThreadPoolExecutor threadPoolExecutor) {
		this.requestFactory = requestFactory;
		this.applicationProperties = applicationProperties;
		this.threadPoolExecutor = threadPoolExecutor;
		this.connectionTimeout = applicationPropertiesService.getPluginProperty(PostReceiveHookConstants.HOOK_CONNECTION_TIMEOUT, 10000);
	}

	private static ThreadPoolExecutor buildExecutor(ApplicationPropertiesService applicationPropertiesService) {

		int threadPoolCoreSize = applicationPropertiesService.getPluginProperty(PostReceiveHookConstants.THREAD_POOL_CORE_SIZE_PROPERTY, 2);
		int threadPoolMaxSize = applicationPropertiesService.getPluginProperty(PostReceiveHookConstants.THREAD_POOL_MAX_SIZE_PROPERTY, 3);
		int queueSize = applicationPropertiesService.getPluginProperty(PostReceiveHookConstants.QUEUE_SIZE_PROPERTY, 1024);

		BlockingQueue<Runnable> runnableQueue = new LinkedBlockingQueue<Runnable>(queueSize);
		ThreadPoolExecutor result = new ThreadPoolExecutor(threadPoolCoreSize, threadPoolMaxSize, 5, TimeUnit.MINUTES, runnableQueue,
				ThreadFactories.namedThreadFactory("WebHookProcessor", Type.DAEMON));

		result.setRejectedExecutionHandler(new RejectedExecutionHandler() {

			@Override
			public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
				if (executor.isShutdown()) {
					logDiscardedChangesets(((RequestFactoryWebHookProcessorWorker) r).getWebHookRequest());

				} else {
					RequestFactoryWebHookProcessorWorker toDiscard = (RequestFactoryWebHookProcessorWorker) executor.getQueue().poll();
					if (toDiscard != null) {
						logDiscardedChangesets(toDiscard.getWebHookRequest());
					}

					// there is a risk that StackOverflow can happened, especially if too many tasks are coming,
					// but implementation is realized as non-blocking implementation, we should not add something to the queue,
					// otherwise it can be unpredictable behavior
					executor.execute(r);
				}
			}

		});

		return result;
	}

	public void processAsync(WebHookRequest webHookRequest) {
		threadPoolExecutor.execute(new RequestFactoryWebHookProcessorWorker(connectionTimeout, requestFactory, webHookRequest,
				applicationProperties));
	}

	@Override
	public void destroy() throws Exception {
		threadPoolExecutor.shutdown();
		if (!threadPoolExecutor.awaitTermination((long) (1.5 * connectionTimeout), TimeUnit.MILLISECONDS)) {
			log.warn("Event dispatcher executor service did not shutdown within the timeout; forcing shutdown");

			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<RequestFactoryWebHookProcessorWorker> toDiscard = (List) threadPoolExecutor.shutdownNow();
			for (RequestFactoryWebHookProcessorWorker toDiscardItem : toDiscard) {
				logDiscardedChangesets(toDiscardItem.getWebHookRequest());
			}

			if (threadPoolExecutor.awaitTermination((long) (1.5 * connectionTimeout), TimeUnit.MICROSECONDS)) {
				// The forced shutdown has brought the executor down. Not ideal, but acceptable
				log.debug("Event dispatcher executor service has been forced to shutdown");
			} else {
				// We can't delay execution indefinitely waiting, so log a warning. The JVM may not shut down
				// if this service does not stop (because it uses non-daemon threads), so this may be helpful
				// in debugging should that happen.
				log.warn("Event dispatcher executor service did not shutdown; it will be abandoned");
			}
		}
	}

	private static void logDiscardedChangesets(final WebHookRequest toDiscard) {
		log.warn(new StringBuilder()
				.append("Discarding WebHook call '")
				.append(URIUtils.sanitize(toDiscard.getHookUri()))
				.append("' for next changesets: (")
				.append(StringUtils.join(Iterables.transform(toDiscard.getRefChanges(), new Function<RefChange, String>() {

					@Override
					public String apply(RefChange input) {
						return "[ Repository: '" + toDiscard.getRepository() + "', RefId: '" + input.getRefId() + "', From Hash: '"
								+ input.getFromHash() + "', To Hash: " + input.getToHash() + "' ]";
					}

				}).iterator(), ", ")).append(") for more information see: ").append(PostReceiveHookConstants.PLUGIN_DOCUMENTATION_URL)
				.toString());
	}
}

package it.com.atlassian.stash.plugin.hook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.http.HttpHeaders;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openqa.selenium.By;

import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.stash.plugin.hook.WebPostReceiveHook;
import com.atlassian.stash.plugin.hook.internal.URIUtils;
import com.atlassian.webdriver.stash.StashTestedProduct;
import com.atlassian.webdriver.stash.element.AuiForm;
import com.atlassian.webdriver.stash.page.StashHomePage;
import com.atlassian.webdriver.stash.page.StashLoginPage;
import com.atlassian.webdriver.stash.page.admin.repo.RepositoryHookDialog;
import com.atlassian.webdriver.stash.page.admin.repo.RepositoryHooksPage;
import com.atlassian.webdriver.stash.page.admin.repo.RepositoryHooksPage.HookRow;

/**
 * Integration test of {@link WebPostReceiveHook}.
 */
public class WebPostReceiveHookTest {

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private static final String HOOK_DISPLAY_NAME = "Post-Receive WebHooks";
	private static final String HOOK_SERVER_CONTEXT_PATH = "/test";
	private static final String HOOK_SERVLET_PATH = "/hook";

	private static final String COMMIT_AUTHOR_NAME = "atlas";
	private static final String COMMIT_AUTHOR_EMAIL = "atlas@atlas";
	private static final String COMMIT_MESSAGE = "Test commit message";

	private static final String BRANCH_TEST_DELETE_BRANCH = "testDeleteBranch";

	private final UsernamePasswordCredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider("admin", "admin");

	private static Server server;
	private static HookServlet hookServlet = new HookServlet();

	private static StashTestedProduct testedProduct;
	private File testRepo;
	private Git git;

	private static final class HookServlet extends HttpServlet {

		private static final long serialVersionUID = 1L;

		private AtomicBoolean received = new AtomicBoolean(false);

		private JSONObject payload;

		private String authorizationHeader;

		public void reset() {
			received.set(false);
			payload = null;
		}

		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			try {
				this.payload = new JSONObject(IOUtils.toString(req.getReader()));
				this.authorizationHeader = req.getHeader(HttpHeaders.AUTHORIZATION);

			} catch (JSONException e) {
				throw new RuntimeException(e);

			}

			received.set(true);
			synchronized (received) {
				received.notify();
			}
		}

		public void waitForReceive(int timeout) {
			try {
				synchronized (received) {
					if (!received.get()) {
						received.wait(timeout);
					}
				}

			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}

			assertTrue("There was no hook received!", received.get());
		}

		public void assertAuthorizationHeader(String expectedAuthorizationHeader) {
			Assert.assertEquals(expectedAuthorizationHeader, authorizationHeader);
		}

		public void assertReceived(RevCommit commit) {
			try {
				JSONArray refChanges = payload.getJSONArray("refChanges");
				assertEquals(1, refChanges.length());

				JSONObject refChange = refChanges.getJSONObject(0);
				assertEquals("refs/heads/master", refChange.getString("refId"));
				assertEquals(commit.getParents()[0].getId().getName(), refChange.getString("fromHash"));
				assertEquals(commit.getId().getName(), refChange.getString("toHash"));
				assertEquals("UPDATE", refChange.getString("type"));

				JSONObject repository = payload.getJSONObject("repository");
				assertEquals(1, repository.getInt("id"));
				assertEquals("rep_1", repository.getString("slug"));
				assertEquals("rep_1", repository.getString("name"));
				assertFalse(repository.getBoolean("public"));

				assertTrue(payload.getJSONObject("changesets").getBoolean("isLastPage"));
				JSONArray changesets = payload.getJSONObject("changesets").getJSONArray("values");

				JSONObject changeset = changesets.getJSONObject(0);
				JSONObject toCommit = changeset.getJSONObject("toCommit");
				assertEquals(commit.getId().getName(), toCommit.getString("id"));
				assertEquals(commit.getFullMessage(), toCommit.getString("message"));
				assertEquals(commit.getCommitTime(), toCommit.getLong("authorTimestamp") / 1000);

				JSONObject author = toCommit.getJSONObject("author");
				assertEquals(commit.getAuthorIdent().getName(), author.getString("name"));
				assertEquals(commit.getAuthorIdent().getEmailAddress(), author.getString("emailAddress"));

				JSONArray changes = changeset.getJSONObject("changes").getJSONArray("values");
				assertEquals(0, changes.length());

			} catch (JSONException e) {
				throw new RuntimeException(e);

			}
		}

		public void assertStripReceived() {
			try {
				assertFalse(payload.getJSONObject("changesets").getBoolean("isLastPage"));

			} catch (JSONException e) {
				throw new RuntimeException(e);

			}
		}

		public void assertDeleteBranch() {
			try {
				assertEquals("DELETE", payload.getJSONArray("refChanges").getJSONObject(0).getString("type"));
				assertEquals(0, payload.getJSONObject("changesets").getJSONArray("values").length());
			} catch (JSONException e) {
				throw new RuntimeException(e);
			}
		}

	}

	@BeforeClass
	public static void beforeClass() {
		testedProduct = TestedProductFactory.create(StashTestedProduct.class);
		testedProduct.visit(StashLoginPage.class).loginAsSysAdmin(StashHomePage.class);

		startTestServer();
	}

	@Before
	public void before() {
		try {
			testRepo = temporaryFolder.newFolder("test-repo");
		} catch (IOException e) {
			throw new RuntimeException(e);

		}

		git = initializeRepository();
		setUpTestBranch(BRANCH_TEST_DELETE_BRANCH);

		startTestServer();
		setUpHookListener();
	}

	@After
	public void after() {
		cleanUpTestBranch(BRANCH_TEST_DELETE_BRANCH);
		hookServlet.reset();
	}

	/**
	 * Cleans test environment.
	 */
	@AfterClass
	public static void afterClass() {
		shutdownServer();
	}

	private static void startTestServer() {
		try {
			server = new Server(0);
			ServletContextHandler servletContextHandler = new ServletContextHandler(server, HOOK_SERVER_CONTEXT_PATH, true, false);
			servletContextHandler.addServlet(new ServletHolder(hookServlet), HOOK_SERVLET_PATH);
			server.start();

		} catch (Exception e) {
			throw new RuntimeException("Unable to start test server:", e);

		}
	}

	private static void shutdownServer() {
		try {
			server.stop();

		} catch (Exception e) {
			throw new RuntimeException("Test server was not properly shutdown:", e);

		}
	}

	private void setUpHookListener() {
		setUpHookListener(resolveHookUrl());
	}

	private void setUpHookListener(String url) {
		RepositoryHooksPage repositoryHooksPage = testedProduct.visit(RepositoryHooksPage.class, "PROJECT_1", "rep_1");

		RepositoryHookDialog configureDialog = resolvePostReceiveHook(repositoryHooksPage).configure();
		AuiForm configureDialogForm = configureDialog.getForm();
		configureDialogForm.setFieldValue(WebPostReceiveHook.URL_SETTINGS_PREFIX + "0", url);
		testedProduct.getTester().getDriver().findElement(By.cssSelector(".button-panel-submit-button")).click();
	}

	private void setUpTestBranch(String branchName) {
		if (branchExists(git, branchName)) {
			branchDelete(git, branchName);
		}
		branchCreate(git, branchName);
	}

	private void cleanUpTestBranch(String branchName) {
		if (branchExists(git, branchName)) {
			branchDelete(git, branchName);
		}
	}

	private HookRow resolvePostReceiveHook(RepositoryHooksPage repositoryHooksPage) {
		for (HookRow hookRow : repositoryHooksPage.getPostReceiveHookRows()) {
			String hookName = hookRow.getName();
			if (hookName.equals(HOOK_DISPLAY_NAME)) {
				return hookRow;
			}
		}

		throw new RuntimeException("There is no " + HOOK_DISPLAY_NAME + "!");
	}

	private String resolveHookUrl() {
		Connector mainConnector = server.getConnectors()[0];
		String host = mainConnector.getHost() != null ? mainConnector.getHost() : "localhost";
		return "http://" + host + ":" + mainConnector.getLocalPort() + HOOK_SERVER_CONTEXT_PATH + HOOK_SERVLET_PATH;
	}

	private String resolveScmUrl() {
		return testedProduct.getProductInstance().getBaseUrl() + "/scm/project_1/rep_1.git";
	}

	/**
	 * Smoke receive hook test.
	 */
	@Test
	public void testReceive() {
		RevCommit commit = commit(git);
		push(git, "master");

		hookServlet.waitForReceive(10000);
		hookServlet.assertReceived(commit);
	}

	@Test
	public void testReceiveWithAuthentication() throws Exception {
		String userInfo = "user:pass";

		setUpHookListener(URIUtils.rewrite(new URI(resolveHookUrl()), "user:pass"));

		RevCommit commit = commit(git);
		push(git, "master");

		hookServlet.waitForReceive(10000);
		hookServlet.assertReceived(commit);
		hookServlet.assertAuthorizationHeader("Basic " + DatatypeConverter.printBase64Binary(userInfo.getBytes()));
 	}

	/**
	 * Test receive of stripped content.
	 */
	@Test
	public void testStripReceive() {
		for (int i = 0; i < 1100; i++) {
			commit(git);
		}
		push(git, "master");

		hookServlet.waitForReceive(30000);
		hookServlet.assertStripReceived();
	}

	/**
	 * Test that delete of branch is proceed correctly.
	 */
	@Test
	public void testDeleteBranch() {
		checkout(git, BRANCH_TEST_DELETE_BRANCH);
		commit(git);
		push(git, BRANCH_TEST_DELETE_BRANCH);
		hookServlet.waitForReceive(10000);
		hookServlet.reset();

		// delete & push
		branchDelete(git, BRANCH_TEST_DELETE_BRANCH);
		hookServlet.waitForReceive(10000);

		hookServlet.assertDeleteBranch();
	}

	private Git initializeRepository() {
		CloneCommand cloneRepository = Git.cloneRepository();

		try {
			return cloneRepository.setDirectory(testRepo).setCredentialsProvider(credentialsProvider).setURI(resolveScmUrl()).call();

		} catch (GitAPIException e) {
			throw new RuntimeException(e);

		}

	}

	private boolean branchExists(Git git, String name) {
		try {
			return git.getRepository().getRef(name) != null || git.getRepository().getRef("origin/" + name) != null;

		} catch (IOException e) {
			throw new RuntimeException(e);

		}
	}

	private void branchCreate(Git git, String name) {
		try {
			git.branchCreate().setName(name).call();

		} catch (GitAPIException e) {
			throw new RuntimeException(e);

		}
	}

	private void branchDelete(Git git, String name) {
		try {
			Ref currentHeadRef = git.getRepository().getRef("HEAD");
			Ref branchRef = git.getRepository().getRef(name);
			Ref branchRemoteRef = git.getRepository().getRef("refs/remotes/origin/" + name);

			if (currentHeadRef != null && branchRef != null && currentHeadRef.getObjectId().equals(branchRef.getObjectId())) {
				checkout(git, "master");
			}

			if (branchRemoteRef != null) {
				push(git, ":" + branchRef.getName());
			}

			if (branchRef != null) {
				git.branchDelete().setBranchNames(branchRef.getName()).setForce(true).call();
			}

		} catch (GitAPIException e) {
			throw new RuntimeException(e);

		} catch (IOException e) {
			throw new RuntimeException(e);

		}
	}

	private void checkout(Git git, String name) {
		try {
			git.checkout().setName(name).call();

		} catch (GitAPIException e) {
			throw new RuntimeException(e);

		}
	}

	private RevCommit commit(Git git) {
		try {
			return git.commit().setMessage(COMMIT_MESSAGE).setAuthor(COMMIT_AUTHOR_NAME, COMMIT_AUTHOR_EMAIL).call();

		} catch (GitAPIException e) {
			throw new RuntimeException(e);
		}
	}

	private void push(Git git, String name) {
		try {
			git.push().setRefSpecs(new RefSpec(name)).setRemote("origin").setCredentialsProvider(credentialsProvider).call();

		} catch (GitAPIException e) {
			throw new RuntimeException(e);

		}
	}

}

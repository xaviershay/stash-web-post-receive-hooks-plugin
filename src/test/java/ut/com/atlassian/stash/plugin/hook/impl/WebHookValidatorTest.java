package ut.com.atlassian.stash.plugin.hook.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.plugin.hook.WebHookValidator;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;
import com.google.common.collect.ImmutableMap;

@RunWith(MockitoJUnitRunner.class)
public class WebHookValidatorTest {

	@Mock
	I18nService i18nService;
	@Mock
	Settings settings;
	@Mock
	Repository repo;
	@Mock
	SettingsValidationErrors errors;

	WebHookValidator testedValidator;

	@Before
	public void setUp() {
		testedValidator = new WebHookValidator(i18nService);
	}

	@Test
	public void testValidateUrlIsValid() {
		setUpForTest("http://localhost/x=y%20", true);
		testedValidator.validate(settings, errors, repo);
		Mockito.verify(errors, Mockito.never()).addFieldError(Mockito.eq("hook.url.0"), Mockito.anyString());
	}

	@Test
	public void testValidateUrlIsInvalidShouldRaiseOneError() {
		setUpForTest("http:/localhost", false);
		executeAndVerify();
	}

	@Test
	public void testValidatePortIsInvalidShouldRaiseOneError() {
		setUpForTest("http://localhost:12x", false);
		executeAndVerify();
	}

	private void executeAndVerify() {
		testedValidator.validate(settings, errors, repo);
	    Mockito.verify(errors, Mockito.times(1)).addFieldError(Mockito.eq("hook.url.0"), Mockito.eq("Invalid URL."));
    }

	private void setUpForTest(String forUrl, boolean isCorrectUrl) {
		Mockito.when(settings.asMap()).thenReturn(ImmutableMap.<String, Object> of("hook.url.0", forUrl));
		Mockito.when(settings.getString("hook.url.0")).thenReturn(forUrl);
		if (!isCorrectUrl) {
			Mockito.when(i18nService.getText(Mockito.eq("stash.webhooks.error.url"), Mockito.eq("Invalid URL.")))
			        .thenReturn("Invalid URL.");
		}
	}

}
